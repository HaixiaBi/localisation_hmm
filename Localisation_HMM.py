# -*- coding: utf-8 -*-
"""
Created on Wed May  6 12:12:20 2020

@author: bhxwzq
"""
import os
import pandas as pd
import matplotlib.pyplot as plt
#from get_data import prepare_seq, plot_seq
from sklearn.datasets import make_spd_matrix
from scipy.stats import multivariate_normal
from sklearn.preprocessing import normalize
from sklearn.metrics import confusion_matrix
from scipy.special import logsumexp
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import StratifiedKFold, GridSearchCV
from sklearn.ensemble import RandomForestClassifier
import numpy as np
from torchnca import NCA
from sklearn.impute import SimpleImputer
from sklearn.svm import SVC
from sklearn.pipeline import Pipeline
import torch

#import hmmlearn
import hmmlearn 
from hmmlearn import hmm

# http://www.cs.cmu.edu/~aarti/Class/10701_Spring14/slides/HMM.pdf

# Extended functions
from functools import reduce

def get_raw_ts_X_y(root_path):    
    dfX = pd.read_csv(os.path.join(root_path, 'Xloc.csv'))
    dfy = pd.read_csv(os.path.join(root_path, 'yloc.csv'))
    X = dfX.values
    y = dfy.values
    
    y = np.array(np.where(y==1))[1,:]-2
    
    return X, y

def preprocess_X_y(X, y):
    
    # Missing value imputation
    imputer1 = SimpleImputer(missing_values = np.nan, strategy = 'mean')
    imputer1 = imputer1.fit( X[:, 0:3])
    X[:,0:3] = imputer1.transform( X[:,0:3])
    
    imputer2 = SimpleImputer(missing_values = np.nan, strategy = 'constant', fill_value=-120)
    #imputer2 = SimpleImputer(missing_values = np.nan, strategy = 'mean')
    imputer2 = imputer2.fit( X[:, 3:7])
    X[:,3:7] = imputer2.transform( X[:,3:7])
    DX = X
    Dy = y
    return DX, Dy

def split_train_test(X, y):
    # Create train and test partitions
    skf = StratifiedKFold(n_splits=5, shuffle=False)
    train_index, test_index = skf.split(X, y).__next__()
    X_train, X_test = X[train_index], X[test_index]
    y_train, y_test = y[train_index], y[test_index]
    return (X_train, y_train), (X_test, y_test)

def get_classifier_grid():

    cv = StratifiedKFold(n_splits=5, shuffle=False)

    models = {'rf': {'model': RandomForestClassifier(),
                     'parameters': {'n_estimators': [200, 250],
                                    'min_samples_leaf': [1, 5, 10]}},
              'svc': {'model': SVC(probability=True),
                      'parameters': {'kernel': ['linear'],
                                      'C': [1, 10, 100, 1000]}},
              }

    classifier_name = 'rf' 

    steps = [('imputer', SimpleImputer(missing_values=np.nan,
                                       strategy='mean')),
             ('scaler', StandardScaler()),
             ('clf', models[classifier_name]['model'])]

    pipeline = Pipeline(steps)

    pipeline_parameters = {'clf__' + key: value for key, value in
                           models[classifier_name]['parameters'].items()}

    clf_grid = GridSearchCV(pipeline, param_grid=pipeline_parameters, cv=cv,
                            refit=True)
    return clf_grid

def print_summary(clf_grid, X_test, y_test):
    import numpy as np
    print('Best parameters are: {}'.format(clf_grid.best_params_))
    print("CV accuracy "+str(np.mean(clf_grid.cv_results_['mean_test_score'])))

    # The best model was fitted on the full training data, here it is tested only
    tt_score = clf_grid.score(X_test, y_test)
    print("Train / test split accuracy "+str(tt_score))

def log_sum_exp(x, y):
    if x>y:
        return x + np.log(1 + np.exp(y-x))
    else:
        return y + np.log(1 + np.exp(x-y))

# def logsumexp(xs):
#     return reduce(log_sum_exp, xs)

class HMM:
    def __init__(self, n_states, start_probs=None, means=None, covs=None, transitions=None):
        self.n_states = n_states
        self.sigma = 1e-3

        self.threshold = 1e-3
        self.emission_likelihood = 0

        self.start_probs = start_probs
        self.means = means
        self.covs = covs
        self.transitions = transitions

        self.mask = np.array([
            [1, 1, 1, 1, 0, 0, 0, 0],
            [1, 1, 1, 1, 0, 0, 0, 0],
            [1, 1, 1, 1, 0, 0, 0, 0],
            [1, 1, 1, 1, 1, 1, 1, 1],
            [0, 0, 0, 0, 1, 1, 1, 1],
            [0, 0, 0, 0, 1, 1, 1, 1],
            [0, 0, 0, 0, 1, 1, 1, 1],
            [0, 0, 0, 0, 1, 1, 1, 1]
        ])

    def _initialise_parameters(self):
        global_mean = np.mean(self.sequences[0], axis=0)
        if self.start_probs is None:
            self.start_probs = np.ones((self.n_states, )) / self.n_states
            # self.start_probs = eln(self.start_probs)

        if self.means is None:
            self.means = [global_mean + np.random.multivariate_normal(
                                np.zeros((self.n_dim, )),
                                1e-2 * np.identity(self.n_dim),
                                1)
                        for _ in range(self.n_states)]
        if self.covs is None:
            # self.covs = [
            #     make_spd_matrix(self.n_dim) for _ in range(self.n_states)
            # ]
            self.covs = [
                1e-2 * np.identity(self.n_dim) for _ in range(self.n_states)
            ]

        if self.transitions is None:
            # self.transitions = np.ones((self.n_states, self.n_states)) / self.n_states
            self.transitions = normalize(np.random.rand(self.n_states, self.n_states), norm='l1', axis=1)
            self.transitions = np.log(self.transitions)

    # def _compute_gaussian_params_seq(self, seq_idx):
    #     for state in range(self.n_states):
    #         weights = np.exp(self.gammas[:, state])
    #         self.means[state] = np.average(self.x, weights=weights.ravel(), axis=0)
    #         self.covs[state] = np.cov(self.x.T, aweights=weights.ravel()) + self.sigma * np.identity(self.n_dim)

    def _update_gaussian_params(self, seq_idx):
        reg = 10 * 1e-1
        sigma = 1e-2
        l = 0.10
        for state in range(self.n_states):
            weights = np.exp(self.gammas[:, state])
            self.means[state] = (1 - l) * np.average(self.sequences[seq_idx], weights=weights.ravel(), axis=0)
            self.covs[state] = np.dot(np.dot(self.sequences[seq_idx].T, np.diag(np.sqrt(weights))), self.sequences[seq_idx]) / (np.sum(weights) + reg) + sigma * np.identity(self.n_dim)

    def eval_likelihood(self, emission, state):
        return multivariate_normal.pdf(emission, self.means[state].ravel(), self.covs[state])

    def _likelihood_emission(self, seq_idx, t):
        return np.array([self.eval_likelihood(self.sequences[seq_idx][t, :], state) for state in range(self.n_states)])

    def _likelihood_emission_decode(self, sequence, t):
        return np.array([self.eval_likelihood(sequence[t, :], state) for state in range(self.n_states)])

    def eval_log_likelihood(self, emission, state):
        return multivariate_normal.logpdf(emission, self.means[state].ravel(), self.covs[state])

    def _log_likelihood_emission(self, seq_idx, t):
        return np.array([self.eval_log_likelihood(self.sequences[seq_idx][t, :], state) for state in range(self.n_states)])

    def _likelihood_sequence(self):
        return np.sum(np.exp(self.alphas[-1, :]))

    def backward(self, seq_idx):
        self.betas = np.zeros((self.n_samples[seq_idx], self.n_states))
        for t in range(self.n_samples[seq_idx]-2, -1, -1):
            loglikelihoods = self._log_likelihood_emission(seq_idx, t+1)
            for i in range(self.n_states):
                self.betas[t, i] = logsumexp([
                    self.betas[t+1, j] +
                    self.transitions[i, j] +
                    loglikelihoods[j]
                    for j in range(self.n_states)
                    ])

    def forward(self, seq_idx):
        self.alphas = np.zeros((self.n_samples[seq_idx], self.n_states))
        self.alphas[0, :] = self._log_likelihood_emission(seq_idx, 0) + np.log(self.start_probs.ravel())

        for t in range(1, self.n_samples[seq_idx]):
            loglikelihoods = self._log_likelihood_emission(seq_idx, t)
            for k in range(self.n_states):
                self.alphas[t, k] = loglikelihoods[k] + \
                    logsumexp([self.alphas[t-1, i] + self.transitions[i, k] for i in range(self.n_states)])

    def traceback(self):
        arr = np.zeros((self.n_samples_curr, ), dtype=int)
        arr[-1] = int(np.argmax(self.vetas[-1, :]))
        for t in range(self.n_samples_curr-2, -1, -1):
            arr[t] = int(np.argmax(np.multiply(self.exp_transitions[:, arr[t+1]], self.vetas[t, :])))
        return arr

    def get_gammas(self, seq_idx):
        self.gammas = np.zeros((self.n_samples[seq_idx], self.n_states))
        self.dens = np.zeros((self.n_samples[seq_idx], ), dtype=int)
        for t in range(self.n_samples[seq_idx]):
            self.gammas[t, :] = self.alphas[t, :] + self.betas[t, :]
            self.dens[t] = logsumexp(self.gammas[t, :].tolist())
            self.gammas[t, :] -= self.dens[t]

    def get_xis(self, seq_idx):
        self.xis = []
        for t in range(1, self.n_samples[seq_idx]):
            xi = np.zeros((self.n_states, self.n_states))
            loglikelihoods = self._log_likelihood_emission(seq_idx, t)
            for i in range(self.n_states):
                for j in range(self.n_states):
                    # xi[i, j] = \
                    #     self.gammas[t-1, i] + \
                    #     self.betas[t, j] - \
                    #     self.betas[t-1, i] + \
                    #     self.transitions[i, j] + \
                    #     loglikelihoods[j]
                    xi[i, j] = \
                        self.alphas[t-1, i] + \
                        self.transitions[i, j] + \
                        self.betas[t, j] + \
                        loglikelihoods[j] - \
                        self.dens[t]
            self.xis.append(xi)

    def update_transitions(self):
        # xis_sum = sum(np.exp(self.xis))
        # gammas_sum = np.sum(np.exp(self.gammas), axis=0)
        # self.transitions = np.log(np.array([
        #     [0.89, 0., 0.11, 0., 0.],
        #     [0.13, 0.73, 0.13, 0., 0.],
        #     [0., 0.07, 0.87, 0.07, 0.],
        #     [0., 0., 0., 0.79, 0.21],
        #     [0., 0., 0.3, 0., 0.7]
        # ]))
        for i in range(self.n_states):
            den = logsumexp([self.gammas[t, i] for t in range(len(self.gammas)-1)])
            for j in range(self.n_states):
                self.transitions[i, j] = \
                    logsumexp([self.xis[t][i, j] for t in range(len(self.xis))]) - den
                # self.transitions[i, j] = xis_sum[i, j] / gammas_sum[i]

        # t = np.multiply(np.exp(self.transitions), self.mask)
        t = np.exp(self.transitions)
        for i in range(self.n_states):
            t[i, i] = max(t[i, i] - 0.30, 0)
        t[np.isnan(t)]=0
        # t = np.around(t, decimals=3)
        # t = normalize(t, axis=1, norm='l1')
        # self.transitions = np.log(t)
        self.transitions = np.log(normalize(t, axis=1, norm='l1'))

    def baum_welch(self):
        for _ in range(1):
            # print(np.around(np.exp(self.transitions), decimals=2))
            # e-step

            for seq_idx in range(self.n_sequences):
                self.forward(seq_idx)
                self.backward(seq_idx)
                self.get_gammas(seq_idx)
                self.get_xis(seq_idx)

                # m-step
                # self.start_probs = self.gammas[0, :]
                self.start_probs = np.exp(self.gammas[0, :])
                self.start_probs += 0.20
                self.start_probs /= np.sum(self.start_probs)
                self.update_transitions()
                self._update_gaussian_params(seq_idx)
            curr = np.sum(np.exp(self.alphas[-1, :]))
            print(curr)

    def viterbi(self, sequence):
        self.exp_transitions = np.exp(self.transitions)
        self.n_samples_curr = len(sequence)
        self.vetas = np.zeros((self.n_samples_curr, self.n_states))
        self.vetas[0, :] = np.multiply(self._likelihood_emission_decode(sequence, 0), self.start_probs.ravel())
        for t in range(1, self.n_samples_curr):
            likelihoods = self._likelihood_emission_decode(sequence, t)
            for k in range(self.n_states):
                self.vetas[t, k] = likelihoods[k] * np.max(np.multiply(self.exp_transitions[:, k].ravel(), self.vetas[t-1, :]))
            # print(self.vetas[t, :])

    def fit_params(self, x, y, params_to_fit='smct'):
        self.n_samples, self.n_dim = x.shape
        sigma = 1e-2
        epsilon = 0.10
        if 's' in params_to_fit:
            self.start_probs = (epsilon / (self.n_states - 1)) * np.ones((self.n_states, ))
            self.start_probs[y[0]] = 1 - epsilon
            self.start_probs /= np.sum(self.start_probs)

        if 'm' in params_to_fit:
            self.means = []
            for state in range(self.n_states):
                locations = np.where(y==state)[0]
                self.means.append(np.mean(x[locations, :], axis=0))

        if 'c' in params_to_fit:
            self.covs = []
            for state in range(self.n_states):
                locations = np.where(y==state)[0]
                self.covs.append(np.cov(x[locations, :].T) + sigma * np.identity(self.n_dim))

        if 't' in params_to_fit:
            # self.transitions = np.log(np.array([
            #     [0.89, 0., 0.11, 0., 0.],
            #     [0.13, 0.73, 0.13, 0., 0.],
            #     [0., 0.07, 0.87, 0.07, 0.],
            #     [0., 0., 0., 0.79, 0.21],
            #     [0., 0., 0.3, 0., 0.7]
            # ]))
            self.transitions = np.zeros((self.n_states, self.n_states))
            for state in range(self.n_states):
                for location in np.where(y[:-1]==state)[0]:
                    self.transitions[state, y[location]] += 1
            # self.transitions += 0.05
            self.transitions = np.log(normalize(normalize(self.transitions, axis=1, norm='l1') + 0.20, axis=1, norm='l1'))

    def fit(self, sequences):
        self.sequences = sequences
        self.n_sequences = len(sequences)
        self.n_dim = sequences[0].shape[1]
        self.n_samples = [len(seq) for seq in sequences]

        self._initialise_parameters()
        self.baum_welch()

    def decode(self, seq):
        self.viterbi(seq)
        return self.traceback()

if __name__ == '__main__':
    root_path = './'
    output_path = './results/'
    Xraw, yraw = get_raw_ts_X_y(root_path)    
    X, y = preprocess_X_y(Xraw, yraw)
    imputer = SimpleImputer(missing_values = np.nan, strategy = 'mean')
    imputer = imputer.fit(X)
    X = imputer.transform(X) 
    
    '''Normalize all the features'''              
    Xnor=X
    for i in range(Xnor.shape[1]):
        ss = StandardScaler()
        scaler  = ss.fit(Xnor[:,i].reshape(-1,1))
        Xnor[:,i] = scaler.transform(Xnor[:,i].reshape(-1,1)).flatten()
    ynor = y
    '''Only get the first 4 dimensions of features'''   
    X = Xnor[:,3:7]
    
    num_hidden_states = 9  #number of hidden states    
    num_observ_states = X.shape[1] #number of observation states
    
    (X_train, y_train), (X_test, y_test) = split_train_test(X, y)
    
    '''Classification'''
    clf_grid = get_classifier_grid()
    clf_grid.fit(X_train, y_train)
    y_prob = clf_grid.predict_proba(X_test)
    y_pred = np.argmax(y_prob, axis=1)    
    cfm = confusion_matrix(y_test, y_pred)    
    print_summary(clf_grid, X_test, y_test)  
    
    #Initialize mean values of hmm model
    MeanV = np.zeros([num_hidden_states, num_observ_states])
    CovarV = np.zeros([num_hidden_states, num_observ_states, num_observ_states])
    StartP = np.zeros([num_hidden_states])
    for i in range(num_hidden_states):
        tmpX = X_train[np.array(np.where(y_train==i))]
        tmpX = tmpX.reshape(tmpX.shape[1], tmpX.shape[2])
        MeanV[i,:] = np.mean(tmpX, axis=0)
        CovarV[i,:,:] = np.cov(np.transpose(tmpX))    
        StartP[i] = len(tmpX)/len(y_train)  

    mdl=HMM(len(np.unique(y_train)))
    mdl.fit_params(X_train,y_train, params_to_fit='smtc')
    mdl.fit([X_train])
    p=mdl.decode(X_train)
    cfm = confusion_matrix(y_train, p)    

    '''Code using sklearn hmm
    #Initialize mean values of hmm model
    MeanV = np.zeros([num_hidden_states, num_observ_states])
    CovarV = np.zeros([num_hidden_states, num_observ_states, num_observ_states])
    StartP = np.zeros([num_hidden_states])
    for i in range(num_hidden_states):
        tmpX = X_train[np.array(np.where(y_train==i))]
        tmpX = tmpX.reshape(tmpX.shape[1], tmpX.shape[2])
        MeanV[i,:] = np.mean(tmpX, axis=0)
        CovarV[i,:,:] = np.cov(np.transpose(tmpX))    
        StartP[i] = len(tmpX)/len(y_train)  
    
    model = hmm.GaussianHMM(n_components=len(np.unique(y_train)), covariance_type="full",n_iter=20, tol=0.01)
    #model.startprob_ = np.array([1/9, 1/9, 1/9, 1/9, 1/9, 1/9, 1/9, 1/9, 1/9])   #n_components
    model.startprob_ = StartP
    model.transmat_ = np.array([[0.8,  0.1,     0,    0,    0,    0, 0.1,   0, 0   ],   #n_components*n_components
                                [0.05, 0.8,  0.05,    0,    0,    0, 0.05,  0, 0.05],                                
                                [0.05, 0.05,  0.8,    0,    0,    0, 0.05,  0, 0.05],                                
                                [   0,   0,     0,  0.8, 0.05, 0.05, 0.05,  0, 0.05],
                                [   0,   0,     0, 0.05,  0.8, 0.05, 0.05,0.05,0   ],
                                [   0,  0.1,    0,    0,    0,  0.8,    0,0.1, 0   ],   
                                [0.04, 0.04, 0.04,    0, 0.04,    0,  0.8,  0, 0.05],
                                [   0,    0,    0,  0.1,  0.1,    0,    0,0.8, 0   ],
                                [0.05, 0.05, 0.05,    0,   0,     0, 0.05,  0, 0.8]])

    model.means_ = MeanV
    model.covars_ = CovarV     
    model.fit(X_train)
    model.fit(X_train)
    model.fit(X_train)    
    
    y_hmm = model.predict(X_train)
    prob_hmm = model.predict_proba(X_train)
    cfm = confusion_matrix(y_train, y_hmm)    
    plt.plot(y_hmm)
    plt.scatter(range(len(y_hmm)), y_hmm)
    plt.scatter(range(len(y_test)), y_test)'''
